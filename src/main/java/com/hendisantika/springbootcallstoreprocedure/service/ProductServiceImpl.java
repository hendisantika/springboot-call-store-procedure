package com.hendisantika.springbootcallstoreprocedure.service;

import com.hendisantika.springbootcallstoreprocedure.entity.Product;
import com.hendisantika.springbootcallstoreprocedure.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-call-store-procedure
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/01/20
 * Time: 05.48
 */
@Transactional
@Service("productService")
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findAllBetweenStoredProcedure(BigDecimal min, BigDecimal max) {
        return productRepository.findAllBetweenStoredProcedure(min, max);
    }

}