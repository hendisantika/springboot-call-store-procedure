package com.hendisantika.springbootcallstoreprocedure.service;

import com.hendisantika.springbootcallstoreprocedure.entity.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-call-store-procedure
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/01/20
 * Time: 05.48
 */
public interface ProductService {

    List<Product> findAllBetweenStoredProcedure(BigDecimal min, BigDecimal max);

}