package com.hendisantika.springbootcallstoreprocedure.config;

import com.hendisantika.springbootcallstoreprocedure.service.ProductService;
import com.hendisantika.springbootcallstoreprocedure.service.ProductServiceImpl;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-call-store-procedure
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/01/20
 * Time: 05.49
 */
@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.hendisantika.springbootcallstoreprocedure.repository"})
@ComponentScan("com.hendisantika")
@PropertySource("classpath:application.properties")
public class JPAConfiguration {

    @Bean
    public ProductService productService() {
        return new ProductServiceImpl();
    }

}