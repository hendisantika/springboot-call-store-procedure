package com.hendisantika.springbootcallstoreprocedure;

import com.hendisantika.springbootcallstoreprocedure.config.JPAConfiguration;
import com.hendisantika.springbootcallstoreprocedure.entity.Product;
import com.hendisantika.springbootcallstoreprocedure.service.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import java.math.BigDecimal;

@SpringBootApplication
public class SpringbootCallStoreProcedureApplication {
    private static Logger logger = LogManager.getLogger(SpringBootApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCallStoreProcedureApplication.class, args);
        try {
            AbstractApplicationContext context = new AnnotationConfigApplicationContext(JPAConfiguration.class);
            ProductService productService = context.getBean(ProductService.class);
            System.out.println("Find product have price between 4 and 8");
            for (Product product : productService.findAllBetweenStoredProcedure(BigDecimal.valueOf(4), BigDecimal.valueOf(8))) {
                logger.info("Id: {}", product.getId());
                logger.info("Name: {}", product.getName());
                logger.info("Price: {}", product.getPrice());
                logger.info("========================");
            }
            context.close();
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }

}
