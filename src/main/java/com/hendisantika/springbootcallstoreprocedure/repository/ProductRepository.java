package com.hendisantika.springbootcallstoreprocedure.repository;

import com.hendisantika.springbootcallstoreprocedure.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-call-store-procedure
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/01/20
 * Time: 05.47
 */
@Repository("productRepository")
public interface ProductRepository extends CrudRepository<Product, Integer> {

    @Query(value = "{call sp_findBetween(:min, :max)}", nativeQuery = true)
    List<Product> findAllBetweenStoredProcedure(@Param("min") BigDecimal min, @Param("max") BigDecimal max);

}