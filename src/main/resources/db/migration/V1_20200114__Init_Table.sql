--
-- Table structure for table `product`
--

CREATE TABLE `product`
(
    `id`          int(11)                              NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name`        varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    `price`       decimal(10, 1)                       NOT NULL,
    `quantity`    int(11)                              NOT NULL,
    `description` text COLLATE utf8_unicode_ci         NOT NULL,
    `photo`       varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    `featured`    tinyint(1)                           NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;